import React, {Component} from 'react';

class ViewContact extends Component {
    render() {
        return (
            <div>
                <div>
                    {this.props.photo ? <img src={this.props.photo} alt="face" height="100" width="auto"/> :
                        <img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-contact-512.png" alt="face" height="100" width="auto"/>}
                </div>
                <div>
                    <p>{this.props.name}</p>
                    <p>Phone: {this.props.phone}</p>
                    <p>Email: {this.props.email}</p>
                    <button onClick={this.props.editClick}>Edit</button>
                    <button onClick={this.props.removeClick}>Remove</button>
                    <button onClick={this.props.closeClick}>Close</button>
                </div>
            </div>
        );
    }
}

export default ViewContact;