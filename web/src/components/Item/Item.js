import React from 'react';
import './Item.css'

const Item = props => {
    return (
        <div className="Item">
            <div className="Info">
                <p>{props.name}</p>
                <button onClick={props.click}>View Contact</button>
            </div>
            <div>
                {props.photo ? <img src={props.photo} alt="face" height="80" width="auto"/> : null}
            </div>
        </div>
    );
};

export default Item;