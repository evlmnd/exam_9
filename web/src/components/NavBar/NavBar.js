import React, {Component} from 'react';
import NavLink from "react-router-dom/es/NavLink";
import './NavBar.css';
import connect from "react-redux/es/connect/connect";

class NavBar extends Component {
    render() {
        return (
            <div className="NavBar">
                <NavLink to="/">Contacts</NavLink>
                <NavLink to="/addcontact" className="Add-Contact" onClick={() => this.props.clearViewedItem()}>Add New
                    Contact</NavLink>
            </div>
        );
    }
}

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        clearViewedItem: () => dispatch({type: 'CLEAR_VIEWED_ITEM'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);