import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import './Form.css';

class Form extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: ''
    };

    valueChanged = (e) => {
        const name = e.target.name;
        this.setState({[name]: e.target.value});
    };

    saveClick = (event) => {
        event.preventDefault();
        const item = {
            name: this.state.name,
            phone: this.state.phone,
            email: this.state.email,
            photo: this.state.photo
        };
        this.props.saveContact(item);
        this.setState({name: '', phone: '', email: '', photo: ''});
        this.props.returnBack();
    };

    componentDidMount() {
        if (this.props.viewedItem) {
            this.setState(this.props.viewedItem);
        }
    }

    render() {
        return (
            <div className="container Form">
                <form>
                    <input type="text" placeholder="Name" name="name" onChange={this.valueChanged}
                           value={this.state.name}/>
                    <input type="text" placeholder="Phone" name="phone" onChange={this.valueChanged}
                           value={this.state.phone}/>
                    <input type="text" placeholder="E-mail" name="email" onChange={this.valueChanged}
                           value={this.state.email}/>
                    <input type="text" placeholder="Photo link" name="photo" onChange={this.valueChanged}
                           value={this.state.photo}/>
                    <div className="Photo">
                        {this.state.photo ? <img src={this.state.photo} alt="face" height="100" width="auto"/> :
                            <img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-contact-512.png" alt="face" height="100" width="auto"/>}
                    </div>
                    <button onClick={this.saveClick}>SAVE</button>
                </form>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        viewedItem: state.viewedItem
    };
};

const mapDispatchToProps = () => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);