const initialState = {
    items: {},
    viewedItem: {},
    toShowModal: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_SUCCESS':
            return {...state, items: action.data};
        case 'ADD_SUCCESS':
            return {...state, items: {...state.items}};
        case 'VIEW_ITEM':
            return{...state, viewedItem: action.item, toShowModal: true};
        case 'CLOSE_MODAL':
            return {
                ...state, toShowModal: false
            };
        case 'CLEAR_VIEWED_ITEM':
            return {...state, viewedItem: {}};
        default:
            return state;
    }
};

export default reducer;