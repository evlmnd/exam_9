import axios from '../../axios-contacts';

export const addRequest = () => ({type: 'ADD_REQUEST'});
export const addSuccess = (data) => ({type: 'ADD_SUCCESS', data});
export const addFailure = () => ({type: 'ADD_FAILURE'});

export const onAddContactToBase = item => {
    return dispatch => {
        dispatch(addRequest());
        axios.post('/items.json', item).then(() => {
            dispatch(addSuccess());
            dispatch(getItemsFromBase());
        }, error => {
            dispatch(addFailure(error));
        })
    }
};

export const getRequest = () => ({type: 'GET_REQUEST'});
export const getSuccess = (data) => ({type: 'GET_SUCCESS', data});
export const getFailure = () => ({type: 'GET_FAILURE'});

export const getItemsFromBase = () => {
    return dispatch => {
        dispatch(getRequest());
        axios.get('/items.json').then((response) => {
            dispatch(getSuccess(response.data));
        }, error => {
            dispatch(getFailure(error));
        })
    }
};

export const editRequest = () => ({type: 'EDIT_REQUEST'});
export const editSuccess = () => ({type: 'EDIT_SUCCESS'});
export const editFailure = () => ({type: 'EDIT_FAILURE'});

export const editItemOnBase = (item) => {
    return dispatch => {
        dispatch(editRequest());
        axios.put('/items/' + item.id + '.json', item.item).then(() => {
            console.log(item);
            dispatch(editSuccess());
            dispatch(getItemsFromBase());
        }, error => {
            dispatch(editFailure(error));
        })
    }
};

export const removeRequest = () => ({type: 'REMOVE_REQUEST'});
export const removeSuccess = () => ({type: 'REMOVE_SUCCESS'});
export const removeFailure = () => ({type: 'REMOVE_FAILURE'});

export const removeItemFromBase = (item) => {
    return dispatch => {
        dispatch(removeRequest());
        axios.delete('/items/' + item.id + '.json').then(() => {
            dispatch(removeSuccess());
            dispatch(getItemsFromBase());
        }, error => {
            dispatch(removeFailure(error));
        })
    }
};