import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import NavBar from "./components/NavBar/NavBar";
import AddForm from "./containers/AddForm/AddForm";
import List from "./containers/List/List";
import EditForm from "./containers/EditForm/EditForm";


class App extends Component {
  render() {
    return (
        <div className="App">
            <NavBar/>
            <div className="container">
                <Switch>
                    <Switch>
                        <Route path="/" exact component={List} />
                        <Route path="/addcontact" exact component={AddForm}/>
                        <Route path="/edit/:id"  component={EditForm}/>
                    </Switch>
                </Switch>
            </div>
        </div>
    );
  }
}

export default App;
