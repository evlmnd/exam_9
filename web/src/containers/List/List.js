import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import Item from "../../components/Item/Item";
import {getItemsFromBase, removeItemFromBase} from "../../store/actions/actions";
import Modal from "../../components/Modal/Modal";
import ViewContact from "../../components/ViewContact/ViewContact";
import './List.css';


class List extends Component {

    clickContact = (key) => {
        const item = {...this.props.items[key], id: key};
        this.props.viewContact(item);
    };

    editClick = (item) => {
        this.props.closeModal();

        this.props.history.push({
            pathname: 'edit/' + item.id
        });
    };

    removeClick = (it) => {
        this.props.closeModal();

        const removingItem = {item: it, id: it.id};
        this.props.removeItem(removingItem);
    };

    closeView = () => {
        this.props.closeModal();
        this.props.clearViewedItem();
    };


    componentDidMount() {
        this.props.getItems();
    }

    render() {
        let items = [];


        if (this.props.items) {
            items = Object.keys(this.props.items).map(key => {
                const itemkey = this.props.items[key];
                return <Item
                    key={key}
                    name={itemkey.name}
                    phone={itemkey.phone}
                    email={itemkey.email}
                    click={() => this.clickContact(key)}
                    photo={itemkey.photo ? itemkey.photo : "https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-contact-512.png"}
                />
            });
        } else {
            items = "Nothing here yet :(";
        }

        const item = this.props.viewedItem;
        const modal = (this.props.toShowModal ? <Modal show={this.props.toShowModal} close={() => this.closeView()}>
            <ViewContact
                name={item.name}
                phone={item.phone}
                email={item.email}
                photo={item.photo}
                closeClick={() => this.closeView()}
                editClick={() => this.editClick(item)}
                removeClick={() => this.removeClick(item)}
            />
        </Modal> : null);

        return (
            <div className="List">
                {items}
                {modal}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    items: state.items,
    viewedItem: state.viewedItem,
    toShowModal: state.toShowModal
});

const mapDispatchToProps = dispatch => ({
    getItems: () => dispatch(getItemsFromBase()),
    viewContact: (item) => dispatch({type: 'VIEW_ITEM', item}),
    closeModal: () => dispatch({type: 'CLOSE_MODAL'}),
    removeItem: (item) => dispatch(removeItemFromBase(item)),
    clearViewedItem: () => dispatch({type: 'CLEAR_VIEWED_ITEM'})
});

export default connect(mapStateToProps, mapDispatchToProps)(List);