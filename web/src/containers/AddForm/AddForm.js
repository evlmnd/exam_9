import React, {Component} from 'react';
import Form from "../../components/Form/Form";
import {connect} from "react-redux";
import {onAddContactToBase} from "../../store/actions/actions";

class AddForm extends Component {

    returnBack = () => {
        this.props.history.push('/');
    };

    render() {
        return (
            <div>
                <Form saveContact={this.props.addContactToBase} returnBack={this.returnBack}/>
            </div>
        );
    }
}

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        addContactToBase: (item) => dispatch(onAddContactToBase(item)),
        clearViewedItem: () => dispatch({type: 'CLEAR_VIEWED_ITEM'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddForm);