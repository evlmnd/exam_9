import React, {Component} from 'react';
import Form from "../../components/Form/Form";
import {connect} from "react-redux";
import {editItemOnBase} from "../../store/actions/actions";

class EditForm extends Component {

    returnBack = () => {
        this.props.history.push('/');
    };

    editedItem = (it) => {
        const itemAndId = {item: it, id: this.props.viewedItem.id};
        this.props.editItem(itemAndId);
    };

    render() {
        return (
            <div>
                <Form saveContact={this.editedItem} returnBack={this.returnBack}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        viewedItem: state.viewedItem
    };
};

const mapDispatchToProps = dispatch => {
    return {
        editItem: (item) => dispatch(editItemOnBase(item))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);