import React, {Component} from 'react';
import {connect} from "react-redux";
import {getItems} from "../../store/actions";
import Item from "../../components/Item/Item";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import image from '../../assets/image.png';


class List extends Component {
    componentDidMount() {
        this.props.getItemsFromBase();
    };

    render() {

        let list;
        if (this.props.items) {
            list = Object.values(this.props.items).map(item => {
                const placedImage = {uri: item.photo};
                return <Item
                    key={item.email}
                    name={item.name}
                    phone={item.phone}
                    photo={item.photo ? placedImage : image}
                    press={() => this.props.viewItem(item)}
                />
            });
        } else {
            list = <Text>Nothing here yet :(</Text>
        }


        return (
            <View>
                <Text style={styles.header}>PhoneBook</Text>
                <ScrollView style={styles.list}>
                    {list}
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 30,
        padding: 30,
    },
    list: {
        flexDirection: 'column',
    },

});

const mapStateToProps = state => {
    return {
        items: state.items
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getItemsFromBase: () => dispatch(getItems()),
        viewItem: (item) => dispatch({type: 'VIEW_ITEM', item})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(List);