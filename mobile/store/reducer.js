const initialState = {
    items: {},
    viewedItem: {},
    toShowModal: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_SUCCESS':
            return {...state, items: action.data};
        case 'VIEW_ITEM':
            console.log(action.item);
            return {...state, viewedItem: {...action.item}, toShowModal: true};
        case 'CLOSE_MODAL':
            return {...state, toShowModal: false};
        default:
            return state;
    }
};

export default reducer;