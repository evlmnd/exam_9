import React, {Component} from 'react';
import {Modal, View, Text, TouchableHighlight, StyleSheet, Image} from "react-native";
import {connect} from "react-redux";
import image from '../../assets/image.png';



class InfoModal extends Component {
    render() {
        const placedImage = {uri: this.props.viewedItem.photo};

        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.toShowModal}
            >
                <View style={styles.modal}>
                    <Image source={this.props.viewedItem.photo ? placedImage : image} style={styles.image}/>
                    <Text style={styles.text}>Name: {this.props.viewedItem.name}</Text>
                    <Text style={styles.text}>Phone: {this.props.viewedItem.phone}</Text>
                    <Text style={styles.text}>E-mail: {this.props.viewedItem.email}</Text>

                    <TouchableHighlight onPress={() => this.props.closeModal()} style={styles.button}>
                        <Text style={styles.buttonText}>Back to list</Text>
                    </TouchableHighlight>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 20
    },
    text: {
        fontSize: 25,
    },
    image: {
        width: 150,
        height: 150,
        marginRight: 10
    },
    button: {
        backgroundColor: 'lightpink',
        padding: 10,
        alignSelf: 'center',
        margin: 20,
        borderRadius: 5
    },
    buttonText: {
        color: 'white',
        fontSize: 30,
    }
});

const mapStateToProps = state => {
    return {
        toShowModal: state.toShowModal,
        viewedItem: state.viewedItem
    };
};

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch({type: 'CLOSE_MODAL'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoModal);