import React, {Component} from 'react';
import { StyleSheet, TouchableOpacity, Text, Image} from 'react-native';

class Item extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.item} onPress={this.props.press}>
                <Image
                    source={this.props.photo} style={styles.image}
                />
                <Text style={styles.name}>{this.props.name}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        padding: 10,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        margin: 5,
        flexDirection: 'row',
    },
    name: {
        fontSize: 20,

    },
    image: {
        width: 40,
        height: 40,
        marginRight: 10
    }
});

export default Item;