import React from 'react';
import {Provider} from "react-redux";
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducer from "./store/reducer";
import List from "./containers/List/List";
import InfoModal from "./components/InfoModal/InfoModal";
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    maincontainer: {
        flex: 1,
    }

});

export default class App extends React.Component {


    render() {

        const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

        const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

        return (
            <Provider store={store} style={styles.maincontainer}>
                <List/>
                <InfoModal/>
            </Provider>

        );
    }
}

